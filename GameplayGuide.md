# Gameplay Guide

This page includes information for some of the larger game-play mods in the `trawzifiedsLOTD` modlist.  This guide is intended for Skyrim players that are only familiar with vanilla-like games, and allow players to read up on new game-play that they can partake in.

## Legacy of the Dragonborn

Legacy of the Dragonborn is one of the most acclaimed expansion sized mods for Skyrim which offers without equal; the most versatile, most expansive and most extensive display space for artifacts the Elder Scrolls series has ever seen! On top of offering a Museum for display of nearly 3500 potential unique items and sets (including supported mods), this mod offers an entirely new guild faction you start from the ground up, “The Explorer’s Society”. It offers dozens of new quest, a handful of major quest arcs and numerous useful and unique mechanics all centered around your home at the Dragonborn Gallery in Solitude.

Legacy will change the way you look at Skyrim, change the way you play the game and give you cause and reason for doing everything you do as the Dragonborn. The museum not only offers display space for your loot, but also tracks various accomplishments with detailed displays, shows what you have achieved and where you have been.

As the story of the mod is designed around the canonical aspect of the player being the Dragonborn, it will potentially break some immersion to otherwise play as a non-Dragonborn. The main quest of Legacy centers around it and depends upon certain vanilla quests establishing you as such.

Links:

* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/11802)
* [Wiki](https://legacy-of-the-dragonborn.fandom.com/wiki/Legacy_of_the_Dragonborn_Wiki)

## Interesting NPCs

Interesting NPCs is a project to add color and life to Skyrim through three-dimensional characters. Each NPC is integrated into the world, with a backstory and an extensive dialogue tree to explore. Many of these characters are fully voiced by a talented team of over 80 voice actors. The dialogue choices allow you to role-play, providing humor and depth to each conversation. You can be a jerk or a jester, a white knight or an assassin, as the most important character is you.

Originally, the goal was to expand 8 followers to include commentary on every quest and location in the game, including bonus conversations during or after major quest-lines. Currently, these followers, whom are dubbed "Super Followers" because they fly and wear capes, have approximately 1000+ lines each. It may be unrealistic to meet this goal, however, as actor interest in performing slave labor can ebb and flow, and even a dozen new lines can often take months to get recorded.

Links:

* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/29194)
* [Wiki](http://3dnpc.com/wiki/interesting-npcs/)

## Know Your Enemy

Know Your Enemy fixes Skyrim's repetitive combat by changing the resistance and susceptibility of different creatures and armor pieces to different types of damage. No longer will you be able to slaughter your way through Skyrim armed with only a single weapon or spell, instead you will need to flexibly adapt your strategy to the current foe.

Here's how it works: Know Your Enemy adds 33 different traits that affect how much an enemy is damaged by different kinds of attacks. Physical attacks are broken down into blades, axes, blunt weapons and bows. Magic attacks are broken down into fire, frost, shock, poison and disease. Each creature or armor piece is given a number of traits that stack to determine its overall resistances and weaknesses.

Links:
* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/13807)

## Timing is Everything

The primary function of this mod is to adjust the starting requirements for quests that are only triggered once the player reaches a certain level. This serves several purposes. For example, you might want to delay the start of Dragonborn or Dawnguard until later in the game, or perhaps you feel that receiving a letter from a Jarl discussing the "fame of your exploits across Skyrim" is a bit odd for your level 9 character who doesn't yet have a single great deed to their name. On the other hand, maybe you've been eyeing that choice piece of land in the Pale, but you can't purchase it until you complete a quest that doesn't start until you reach level 22. Maybe you want a specific follower, weapon, or reward that only comes from a later quest, or perhaps you're just seeking the challenge of completing higher-level quests at a relatively early level. Or maybe you're planning to use a character past level 50 and simply want to spread the quests out across more levels to add further interest to late-stage gameplay. Earlier or later, whatever your reasons, it's entirely up to you.

Links:

* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/25464)

## Not so Fast Main Quest

Introduces pauses in the otherwise hectic pace of the main quest and alters a few other plot points along the way. The actual time delays and whether the Season Unending negotiations are skipped can be configured.

Links:

* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/2475)

## Not so Fast Mage Guild

Introduces two pauses in the rather short mage guild quest line at the College of Winterhold and allows for customizing the start of the three apprentices' quests. The actual time delays can be configured.

Links:

* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/5686)

## Wintersun

Wintersun adds religion and worship. Praying, worship at shrines and adhering to the tenets of your deity strengthens your bond with the deity and eventually grants divine powers. In addition to the divines and daedric princes, the mod adds many other deities from Elder Scrolls lore, each with their own shrines in the world.

Links:

* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/22506)

## Class Overhaul Re-Imagined

A complete overhaul and re-imagination of classes. Develop your character through a "series" of classes, further specializing as you go. The options available depend upon how you play and develop your character. If you want to be the evilest of dark Paladins... play as one and the appropriate powers will become available. Rather adventure as a Bard buffing your allies and debuffing your enemies... so be it.

Links:

* [Nexus](https://www.nexusmods.com/skyrimspecialedition/mods/24808)
