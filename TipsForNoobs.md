 # Tips for Noobs

 If, like me, you feel a bit overwhelmed by all the changes made from this modlist, I've compiled a few tips I think may help you make your Trawzified journey go a little smoother.  First are some general tips for playing, followed by several notes from some of the most prominent mods affecting gameplay.

 ## General Tips

 - Save plenty and often.  Autosaves do not exist.

   - Save before entering a dungeon/cell you know you could die in.  You don't want to reload from the same cell, as this can corrupt your save.

   - Think of the door to a dungeon (or next part of the dungeon) as a save point and do so before entering.

   - To my knowledge, it is unclear exactly how much reloading from the same cell will affect your save, but repeatedly doing this WILL.

 - If you decide you want to change the way your character looks via console, that's fine. Do NOT change your race or gender.

 - Hotkeys are kind of interesting. First, you have [EasyWheel] which is useful for commonly used powers added by select mods.  Then, you have the default favorites menu.  I find this to be best used for clothing/armor as well as potions/poisons.  Finally, [Serio's Cycle Hotkeys] is best for managing your weapons, shields, spells, shouts and powers. I recommend making the setter command a combination (eg ctrl+b).  Further, as the inventory and favorite menus no longer pause the action, utilizing these hotkeys is more important than ever ;)
 
 ## Mod Specific Tips

 I've also included links to the original mods making these changes if you'd care to see a more extensive list of their changes.  These are simply the ones that made me scratch my head until I determined their origin.

 ### [Legacy of the Dragonborn]

 This is the big one. The centerpiece of the modlist.  This mod adds a museum to Solitude in which you can display artifacts, gemstones, books, and other trophies from your various exploits.    

 - Save before using the display sorting chest. I've crashed a few times losing a few hours of progress.

 - Wait 60 seconds between taking new blessings for shrine and standing stone displays to update (to prevent confusion at The Temple of the Divines and The Guardian Stones)

 ### [Class Overhaul Re-Imagined (CORI)]

 - When you first choose your path, you will be stronger over time if you remain on that path, though you do have the option to switch paths.

 - Sometimes, you'll be prompted to choose a path seemingly without the option you're looking for.  This is because you haven't yet met the skill requirements for that option. Keep leveling your skills, and eventually you'll find the options you want.

 ### [Complete Alchemy and Cooking Overhaul]

 - Saving and reloading (remember to change cell) stacks player-made potions/poisons of identical effect

 - Some ingredients are no longer ingredients, but food supplies (human flesh, charred skeever meat, etc). Furthermore, some ingredients you may be used to getting normally (eg Eye of Sabre Cat) require certain perks or equipment before they can be collected. -Also trying to figure out salmon roe.-

 - Consuming two potions with the same effect does not stack (eg 2 weak potions of healing worth 25 health points taken simultaneously will still only heal 25 points)

 - Diseases must be cured with a potion of cure disease. Ingredients and shrines will not help you.

 - To keep naming conventions simple, Potion = 1 effect, Draught = 2 effects, and Elixir = 3 effects

 - The newly added Alchemist's Retort can be used to create bombs, weapon-coating waxes, distilled potions and more.  These can be carried with the player for use on-the-fly or found at alchemy stations.

 - The newly added Alchemist's Crucible can be used to standardize player-made potions and poisons (in most cases weakening them).

   - This allows the now-standardized potions to be used as ingredients for distillation to make stronger poisons or weapon waxes, which, while not as strong per hit, allow for more consecutive hits of poison damage when applied to a weapon.

   - These can only be bought or found and carried by the player.
 
 - Players wishing to become a filthy vampire cannot heal using conventional healing potions, but can instead make potions of blood to heal.

 ### [Complete Crafting Overhaul Remastered]

 - Non-joinable faction weapons and armor can be taken to the smelter and melted to learn their properties, allowing you to craft those weapons and armor yourself once you reach the necessary threshold for that type of weapon/armor.

 ### [Honed Metal]

 - Unfavorite and unequip items to be upgraded by the smith, to keep them showing up on your player model.

 - When having your items crafted, upgraded or enchanted, the smith/enchanter will generally have plenty of common components, but not the rarer ones. You will have to provide these rarer components (eg Ebony ingots, Malachite ingots, grand soul gems, etc)

 ### [Know Your Enemy]

 - Makes certain enemies immune to certain kinds of damage.  Most of these make sense from a conventional fantasy perspective.  When fighting mobs, think about what kind of weapon/spell would actually be effective at dispatching them.

 - Enemies with armor also gain resistances and weaknesses depending on the type of their armor.  Again think about what kind of weapon/spell would be effective against that armor type.

 - Don't look at the changes at the modpage.  That's cheating >:(

 ### [Moonlight Tales] 

 Biggest thing to note here is to be wary of the moon. As it approaches full moon, you have a higher chance of transforming.

 ### [Pumping Iron]

 - As your character better at various combat skills, your male character model's muscles will grow, and your female character model's... *assets* will.
 
 - To note the most growth over time, set the weight slider during character creation to 0.

 ### [ESF - Companions], [Not So Fast MG], [Not So Fast MQ], [Thieves Guild Requirements], and [Timing is Everything]

 These mods change the requirements for various quests for most of the major plotlines of the game.

  - [ESF - Companinons] and [Timing is Everything] both require your character to be at a certain level for various quests to progress.

  - [ESF - Companions], [Not So Fast MG], and [Not So Fast MQ] all require a certain amount of in-game time for various quests to progress.

  - [ESF - Companions] and [Thieves Guild Requirements] both require various skills to be at a certain level for various quests to progress.




[comment]: # (Link References)

[Class Overhaul Re-Imagined (CORI)]: https://www.nexusmods.com/skyrimspecialedition/mods/24808

[Complete Alchemy and Cooking Overhaul]: https://www.nexusmods.com/skyrimspecialedition/mods/19924

[Complete Crafting Overhaul Remastered]: https://www.nexusmods.com/skyrimspecialedition/mods/28608

[EasyWheel]: https://www.nexusmods.com/skyrimspecialedition/mods/22934

[ESF - Companions]: https://www.nexusmods.com/skyrim/mods/22650

[Honed Metal]: https://www.nexusmods.com/skyrimspecialedition/mods/12885

[Know Your Enemy]: https://www.nexusmods.com/skyrimspecialedition/mods/13807

[Legacy of the Dragonborn]: https://www.nexusmods.com/skyrimspecialedition/mods/11802

[Moonlight Tales]: https://www.nexusmods.com/skyrimspecialedition/mods/2803

[Not So Fast MG]: https://www.nexusmods.com/skyrimspecialedition/mods/5686

[Not So Fast MQ]: https://www.nexusmods.com/skyrimspecialedition/mods/2475

[Pumping Iron]: https://www.nexusmods.com/skyrimspecialedition/mods/13434

[Serio's Cycle Hotkeys]: https://www.nexusmods.com/skyrimspecialedition/mods/27184

[Thieves Guild Requirements]: https://www.nexusmods.com/skyrimspecialedition/mods/33256

[Timing is Everything]: https://www.nexusmods.com/skyrimspecialedition/mods/25464
